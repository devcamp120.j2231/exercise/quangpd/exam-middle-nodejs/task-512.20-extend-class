import { ConNguoi } from "./conNguoi.js";

class CongNhan extends ConNguoi{
    nganhNghe;
    noiLamViec;
    luong;

    constructor(hoTen,ngaySinh,queQuan,nganhNghe,noiLamViec,luong){
        super(hoTen,ngaySinh,queQuan);

        this.nganhNghe = nganhNghe;
        this.noiLamViec = noiLamViec;
        this.luong = luong;
    }

    congNhanInfo(){
        return `Ngành Nghề  : ${this.nganhNghe} Nơi làm việc : ${this.noiLamViec} Lương : ${this.luong}`
    }
}

export { CongNhan }
import { ConNguoi } from "./conNguoi.js";
import { HocSinh } from "./hocSinh.js";

class SinhVien extends HocSinh {
    chuyenNganh;
    mssv;

    constructor(hoTen,ngaySinh,queQuan,tenTruong,lop,sdt,chuyenNganh,mssv){
        super(hoTen,ngaySinh,queQuan,tenTruong,lop,sdt);

        this.chuyenNganh = chuyenNganh;
        this.mssv = mssv;
    }

    sinhVienInfo(){
        return `Chuyên Ngành : ${this.chuyenNganh} Mã Số Sinh Viên : ${this.mssv}`
    }
}

export { SinhVien}
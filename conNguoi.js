class ConNguoi {
    hoTen;
    ngaySinh;
    queQuan;

    constructor(hoTen,ngaySinh,queQuan){
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.queQuan = queQuan;
    }

    fullInfo(){
        return `Tên : ${this.hoTen} Ngày Sinh : ${this.ngaySinh} Quê Quán : ${this.queQuan}`
    }
}

export {ConNguoi}
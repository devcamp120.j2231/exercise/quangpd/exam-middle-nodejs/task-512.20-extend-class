import { ConNguoi } from "./conNguoi.js";

class HocSinh extends ConNguoi{
    tenTruong;
    lop;
    sdt;

    constructor(hoTen,ngaySinh,queQuan,tenTruong,lop,sdt){
        super(hoTen,ngaySinh,queQuan);
        this.tenTruong = tenTruong;
        this.lop = lop;
        this.sdt = sdt;
    }

    schoolInfo(){
        return `Tên Trường : ${this.tenTruong} Lớp : ${this.lop} Số Điện Thoại : ${this.sdt}`
    }
}

export {HocSinh}
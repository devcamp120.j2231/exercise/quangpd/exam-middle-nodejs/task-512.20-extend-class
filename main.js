import { ConNguoi } from "./conNguoi.js";
import { HocSinh } from "./hocSinh.js";
import { SinhVien } from "./sinhVien.js";
import { CongNhan } from "./congNhan.js";


//Con Người
console.log("----Con Người----");
let newConNguoi = new ConNguoi("Phạm Quang","18/02/1998","Hà Nội");
console.log(newConNguoi.fullInfo());
console.log(newConNguoi instanceof ConNguoi);



//Học Sinh 
console.log("----Học Sinh----");
let newHocSinh = new HocSinh("Hà Hoàng" ,"10/11/2000","Hưng Yên","Đại Học Khoa Học Tự Nhiên","VIP","0869916935");
console.log(newHocSinh.fullInfo());
console.log(newHocSinh.schoolInfo());
console.log(newHocSinh instanceof ConNguoi);
console.log(newHocSinh instanceof HocSinh);

//Sinh viên
console.log("----Sinh Viên----");
let newSinhVien = new SinhVien("BBoy Blond","18/02/1998","Hà Nội","Đại Học Công Nghiệp","CDDCNTT2-K18","0877979735","Công Nghệ Thông Tin","2018069");
console.log(newSinhVien.fullInfo());
console.log(newSinhVien.schoolInfo());
console.log(newSinhVien.sinhVienInfo());
console.log(newSinhVien instanceof ConNguoi);
console.log(newSinhVien instanceof HocSinh);
console.log(newSinhVien instanceof SinhVien);

//Công nhân
console.log("----Công Nhân----");
let newCongNhan = new CongNhan("Công Văn Nhân","11/23/1987","Việt Nam","TechLead","Hà Nội","15000000");
console.log(newCongNhan.fullInfo());
console.log(newCongNhan.congNhanInfo());
console.log(newCongNhan instanceof ConNguoi);
console.log(newCongNhan instanceof CongNhan);